namespace TournamentData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class statusUpdate : DbMigration
    {
        public override void Up()
        {
            CreateIndex("Tournament.EventDetailStatus", "EventDetailStatusName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("Tournament.EventDetailStatus", new[] { "EventDetailStatusName" });
        }
    }
}
