namespace TournamentData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ColumnUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Tournament.EventDetails", "EventDetailStatus_EventDetailStatusID", "Tournament.EventDetailStatus");
            DropForeignKey("Tournament.EventDetails", "Event_EventID", "Tournament.Events");
            DropForeignKey("Tournament.Events", "Tournament_TournamentID", "Tournament.Tournaments");
            DropIndex("Tournament.EventDetails", new[] { "EventDetailStatus_EventDetailStatusID" });
            DropIndex("Tournament.EventDetails", new[] { "Event_EventID" });
            DropIndex("Tournament.Events", new[] { "Tournament_TournamentID" });
            DropColumn("Tournament.EventDetails", "EventDetailStatus_EventDetailStatusID");
            DropColumn("Tournament.EventDetails", "Event_EventID");
            DropColumn("Tournament.Events", "Tournament_TournamentID");
        }
        
        public override void Down()
        {
            AddColumn("Tournament.Events", "Tournament_TournamentID", c => c.Long());
            AddColumn("Tournament.EventDetails", "Event_EventID", c => c.Long());
            AddColumn("Tournament.EventDetails", "EventDetailStatus_EventDetailStatusID", c => c.Short());
            CreateIndex("Tournament.Events", "Tournament_TournamentID");
            CreateIndex("Tournament.EventDetails", "Event_EventID");
            CreateIndex("Tournament.EventDetails", "EventDetailStatus_EventDetailStatusID");
            AddForeignKey("Tournament.Events", "Tournament_TournamentID", "Tournament.Tournaments", "TournamentID");
            AddForeignKey("Tournament.EventDetails", "Event_EventID", "Tournament.Events", "EventID");
            AddForeignKey("Tournament.EventDetails", "EventDetailStatus_EventDetailStatusID", "Tournament.EventDetailStatus", "EventDetailStatusID");
        }
    }
}
