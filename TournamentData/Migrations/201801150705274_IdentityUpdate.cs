namespace TournamentData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdentityUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Tournament.EventDetails",
                c => new
                    {
                        EventDetailID = c.Long(nullable: false, identity: true),
                        FK_EventID = c.Long(nullable: false),
                        FK_EventDetailStatusID = c.Short(nullable: false),
                        EventDetailName = c.String(nullable: false, maxLength: 50, unicode: false),
                        EventDetailNumber = c.Short(nullable: false),
                        EventDetailOdd = c.Decimal(nullable: false, precision: 18, scale: 7),
                        FinishingPosition = c.Short(nullable: false),
                        FirstTimer = c.Boolean(nullable: false),
                        EventDetailStatus_EventDetailStatusID = c.Short(),
                        Event_EventID = c.Long(),
                    })
                .PrimaryKey(t => t.EventDetailID)
                .ForeignKey("Tournament.EventDetailStatus", t => t.EventDetailStatus_EventDetailStatusID)
                .ForeignKey("Tournament.Events", t => t.Event_EventID)
                .Index(t => t.EventDetailStatus_EventDetailStatusID)
                .Index(t => t.Event_EventID);
            
            CreateTable(
                "Tournament.EventDetailStatus",
                c => new
                    {
                        EventDetailStatusID = c.Short(nullable: false, identity: true),
                        EventDetailStatusName = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.EventDetailStatusID);
            
            CreateTable(
                "Tournament.Events",
                c => new
                    {
                        EventID = c.Long(nullable: false, identity: true),
                        FK_TournamentID = c.Long(nullable: false),
                        EventName = c.String(nullable: false, maxLength: 100, unicode: false),
                        EventNumber = c.Short(nullable: false),
                        EventDateTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EventEndDateTime = c.DateTime(precision: 7, storeType: "datetime2"),
                        AutoClose = c.Boolean(nullable: false),
                        Tournament_TournamentID = c.Long(),
                    })
                .PrimaryKey(t => t.EventID)
                .ForeignKey("Tournament.Tournaments", t => t.Tournament_TournamentID)
                .Index(t => t.Tournament_TournamentID);
            
            CreateTable(
                "Tournament.Tournaments",
                c => new
                    {
                        TournamentID = c.Long(nullable: false, identity: true),
                        TournamentName = c.String(nullable: false, maxLength: 200, unicode: false),
                    })
                .PrimaryKey(t => t.TournamentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Tournament.Events", "Tournament_TournamentID", "Tournament.Tournaments");
            DropForeignKey("Tournament.EventDetails", "Event_EventID", "Tournament.Events");
            DropForeignKey("Tournament.EventDetails", "EventDetailStatus_EventDetailStatusID", "Tournament.EventDetailStatus");
            DropIndex("Tournament.Events", new[] { "Tournament_TournamentID" });
            DropIndex("Tournament.EventDetails", new[] { "Event_EventID" });
            DropIndex("Tournament.EventDetails", new[] { "EventDetailStatus_EventDetailStatusID" });
            DropTable("Tournament.Tournaments");
            DropTable("Tournament.Events");
            DropTable("Tournament.EventDetailStatus");
            DropTable("Tournament.EventDetails");
        }
    }
}
