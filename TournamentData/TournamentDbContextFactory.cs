﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace TournamentData
{
    public class TournamentDbContextFactory : IDbContextFactory<TournamentDbContext>
    {
        public TournamentDbContext Create()
        {
            var connstring = ConfigurationManager.ConnectionStrings["ConferenceDBConnectionString"].ConnectionString;
            return new TournamentDbContext(connstring);
        }
    }
}
