﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentModels.Domains;

namespace TournamentData
{
    public class TournamentDbContext : DbContext
    {
        public TournamentDbContext(string connstring) : base(connstring)
        {

        }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventDetail> EventDetails { get; set; }
        public DbSet<EventDetailStatus> EventDetailsStatus { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Tournament");
            modelBuilder.Entity<EventDetail>().Property(x => x.EventDetailOdd).HasPrecision(18, 7);
            base.OnModelCreating(modelBuilder);
            
        }
    }
}
