﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TournamentGenericRepository
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        DbContext _mContext;
        DbSet<TEntity> _mDbSet;

        public GenericRepository(DbContext context)
        {
            _mContext = context;
            _mDbSet = context.Set<TEntity>();
        }
        public DbContext GetDbContext { get { return _mContext; } }

        // Gets all objects of Type TEntity
        public async Task<IEnumerable<TEntity>> All()
        {
            return await _mDbSet.AsNoTracking().ToListAsync();
        }
        
        // Get list of objects based on criteria 
        public async Task<IEnumerable<TEntity>> FindBy(Expression<Func<TEntity,bool>> predicate)
        {
            var results = await _mDbSet.AsNoTracking().Where(predicate).ToListAsync();
            return results;
        }
        // Find the entity using primary key property
        public async Task<TEntity> FindByKey(int id)
        {
            Expression<Func<TEntity, bool>> lambda = Utilities.BuildLamdaByFindKey<TEntity>(id);
            return await _mDbSet.SingleOrDefaultAsync(lambda);
        }
        // Insert an Entity
        public async Task Insert(TEntity entity)
        {
            _mDbSet.Add(entity);
            await _mContext.SaveChangesAsync();
        }
        // Update an Entity
        public async Task Update(TEntity entity)
        {
            _mDbSet.Attach(entity);
            _mContext.Entry(entity).State = EntityState.Modified;
            await _mContext.SaveChangesAsync();
        }
        // Delete an Entity
        public async Task Delete(TEntity entity)
        {
            _mDbSet.Attach(entity);
            _mDbSet.Remove(entity);
            await _mContext.SaveChangesAsync();
        }
    }
}
