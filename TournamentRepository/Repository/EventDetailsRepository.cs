﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentData;
using TournamentModels.Domains;
using TournamentRepository.IRepository;

namespace TournamentRepository.Repository
{
    public class EventDetailsRepository : IEventDetailRepository
    {
        private RepositoryInitializer<EventDetail> _repository;

        public EventDetailsRepository(TournamentDbContext tournamentDbContext)
        {
            _repository = new RepositoryInitializer<EventDetail>(tournamentDbContext);
        }
        public async Task DeleteEventDetail(EventDetail eventDetail)
        {
            await _repository.GenericRepository.Delete(eventDetail);
        }

        public async Task<IEnumerable<EventDetail>> GetAllEventDetails()
        {
            return await _repository.GenericRepository.All();
        }

        public async Task<IEnumerable<EventDetail>> GetEventDetailByEventDetailStatusId(int id)
        {
            return await _repository.GenericRepository.FindBy(c => c.FK_EventDetailStatusID == id);
        }

        public async Task<IEnumerable<EventDetail>> GetEventDetailByEventId(int id)
        {
            return await _repository.GenericRepository.FindBy(c => c.FK_EventID == id);
        }

        public async Task<EventDetail> GetEventDetailById(int id)
        {
            return await _repository.GenericRepository.FindByKey(id);
        }

        public async Task InsertEventDetail(EventDetail eventDetail)
        {
            await _repository.GenericRepository.Insert(eventDetail);
        }

        public async Task UpdateEventDetail(EventDetail eventDetail)
        {
            await _repository.GenericRepository.Update(eventDetail);
        }
    }
}
