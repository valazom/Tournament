﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentData;
using TournamentGenericRepository;
using TournamentModels.Domains;
using TournamentRepository.IRepository;

namespace TournamentRepository.Repository
{
    public class TournamentRepository : ITournamentRepository
    {
        private RepositoryInitializer<Tournament> _repository;

        public TournamentRepository(TournamentDbContext tournamentDbContext)
        {
            _repository = new RepositoryInitializer<Tournament>(tournamentDbContext);
        }
        public async Task DeleteTournament(Tournament tournament)
        {
            await _repository.GenericRepository.Delete(tournament);
        }

        public async Task<IEnumerable<Tournament>> GetAllTournaments()
        {
            return await _repository.GenericRepository.All();
        }

        public async Task<Tournament> GetTournamentById(int id)
        {
            var result = await _repository.GenericRepository.FindByKey(id);
            return result;
        }

        public async Task InsertTournament(Tournament tournament)
        {
            await _repository.GenericRepository.Insert(tournament);
        }

        public async Task UpdateTournament(Tournament tournament)
        {
            await _repository.GenericRepository.Update(tournament);
        }
    }
}
