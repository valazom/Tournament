﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentData;
using TournamentModels.Domains;
using TournamentRepository.IRepository;

namespace TournamentRepository.Repository
{
    public class EventDetailStatusRepository : IEventStatusRepository
    {
        private RepositoryInitializer<EventDetailStatus> _repository;

        public EventDetailStatusRepository(TournamentDbContext tournamentDbContext)
        {
            _repository = new RepositoryInitializer<EventDetailStatus>(tournamentDbContext);
        }
        public async Task DeleteEventDetailStatus(EventDetailStatus eventDetailStatus)
        {
            await _repository.GenericRepository.Delete(eventDetailStatus);
        }

        public async Task<IEnumerable<EventDetailStatus>> GetAllEventStatus()
        {
            return await _repository.GenericRepository.All();
        }

        public async Task<EventDetailStatus> GetEventDetailStatusById(Int16 id)
        {
            return (await _repository.GenericRepository.FindBy(c => c.EventDetailStatusID == id)).SingleOrDefault();
        }

        public async Task InsertEventStatus(EventDetailStatus eventStatus)
        {
            await _repository.GenericRepository.Insert(eventStatus);
        }

        public async Task UpdateEventDetailStatus(EventDetailStatus eventDetailStatus)
        {
            await _repository.GenericRepository.Update(eventDetailStatus);
        }
    }
}
