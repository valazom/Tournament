﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentData;
using TournamentModels.Domains;
using TournamentRepository.IRepository;

namespace TournamentRepository.Repository
{
    public class EventRepository : IEventRepository
    {
        private RepositoryInitializer<Event> _repository;

        public EventRepository(TournamentDbContext tournamentDbContext)
        {
            _repository = new RepositoryInitializer<Event>(tournamentDbContext);
        }
        public async Task DeleteEvent(Event Event)
        {
            await _repository.GenericRepository.Delete(Event);
        }

        public async Task<IEnumerable<Event>> GetAllEvents()
        {
            return await _repository.GenericRepository.All();
        }

        public async Task<Event> GetEventById(int id)
        {
            return await _repository.GenericRepository.FindByKey(id);
        }

        public async Task<IEnumerable<Event>> GetEventsByTournamentId(int id)
        {
            return await _repository.GenericRepository.FindBy(c => c.FK_TournamentID == id);
        }

        public async Task InsertEvent(Event Event)
        {
            await _repository.GenericRepository.Insert(Event);
        }

        public async Task UpdateEvent(Event Event)
        {
            await _repository.GenericRepository.Update(Event);
        }
    }
}
