﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentModels.Domains;

namespace TournamentRepository.IRepository
{
    public interface IEventStatusRepository
    {
        Task InsertEventStatus(EventDetailStatus eventStatus);
        Task<EventDetailStatus> GetEventDetailStatusById(Int16 id);
        Task<IEnumerable<EventDetailStatus>> GetAllEventStatus();
        Task UpdateEventDetailStatus(EventDetailStatus eventDetailStatus);
        Task DeleteEventDetailStatus(EventDetailStatus eventDetailStatus);

    }
}
