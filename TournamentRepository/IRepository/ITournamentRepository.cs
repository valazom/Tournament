﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentModels.Domains;

namespace TournamentRepository.IRepository
{
    public interface ITournamentRepository
    {
        Task InsertTournament(Tournament tournament);
        Task<Tournament> GetTournamentById(int id);
        Task<IEnumerable<Tournament>> GetAllTournaments();
        Task UpdateTournament(Tournament tournament);
        Task DeleteTournament(Tournament tournament);
    }
}
