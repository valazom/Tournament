﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentModels.Domains;

namespace TournamentRepository.IRepository
{
    public interface IEventRepository
    {
        Task InsertEvent(Event Event);
        Task<Event> GetEventById(int id);
        Task<IEnumerable<Event>> GetEventsByTournamentId(int id);
        Task<IEnumerable<Event>> GetAllEvents();
        Task UpdateEvent(Event Event);
        Task DeleteEvent(Event Event);
    }
}
