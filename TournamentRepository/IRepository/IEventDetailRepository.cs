﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentModels.Domains;

namespace TournamentRepository.IRepository
{
    public interface IEventDetailRepository
    {
        Task InsertEventDetail(EventDetail eventDetail);
        Task<EventDetail> GetEventDetailById(int id);
        Task<IEnumerable<EventDetail>> GetAllEventDetails();
        Task<IEnumerable<EventDetail>> GetEventDetailByEventId(int id);
        Task<IEnumerable<EventDetail>> GetEventDetailByEventDetailStatusId(int id);
        Task UpdateEventDetail(EventDetail eventDetail);
        Task DeleteEventDetail(EventDetail eventDetail);
    }
}
