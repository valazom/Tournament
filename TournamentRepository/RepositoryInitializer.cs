﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TournamentData;
using TournamentGenericRepository;

namespace TournamentRepository
{
    public class RepositoryInitializer<TEnity> where TEnity : class
    {
        public RepositoryInitializer(DbContext Context)
        {
            GenericRepository = new GenericRepository<TEnity>(Context);
        }
        public GenericRepository<TEnity> GenericRepository { get; set; }
    }
}
