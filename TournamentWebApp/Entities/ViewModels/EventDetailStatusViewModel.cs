﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentWebApp.Entities.ViewModels
{
    public class EventDetailStatusViewModel
    {
        [Required]
        [StringLength(50)]
        public string EventDetailStatusName { get; set; }
    }
}
