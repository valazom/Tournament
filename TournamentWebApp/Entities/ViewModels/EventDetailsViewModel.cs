﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentWebApp.Entities.ViewModels
{
    public class EventDetailsViewModel
    {
        [Required]
        public string EventDetailName { get; set; }
        [Required]
        public int EventDetailNumber { get; set; }
        [Required]
        public decimal EventDetailOdd { get; set; }
        public short FinishingPosition { get; set; }
        [Required]
        public bool FirstTimer { get; set; }
    }
}
