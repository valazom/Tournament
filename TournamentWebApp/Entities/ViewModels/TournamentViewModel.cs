﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentWebApp.Entities.ViewModels
{
    public class TournamentViewModel
    {
        [Required]
        [StringLength(200)]
        public string TournamentName { get; set; }
    }
}
