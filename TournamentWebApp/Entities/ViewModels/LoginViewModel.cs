﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentWebApp.Entities.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage ="Username cannot be empty")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password cannot be empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Description = "Remember me")]
        public bool rememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}
