﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentWebApp.Entities.ViewModels
{
    public class EventViewModel
    {
        [Required]
        public string EventName { get; set; }
        [Required]
        public int EventNumber { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EventDateTime { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime EventEndDateTime { get; set; }
        [Required]
        public bool AutoClose { get; set; }
    }
}
