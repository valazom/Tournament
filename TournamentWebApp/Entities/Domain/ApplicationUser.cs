﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentWebApp.Entities.Domain
{
    public class ApplicationUser : IdentityUser
    {
    }
}
