﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TournamentData;
using TournamentRepository.IRepository;
using TournamentWebApp.Entities.Data;
using TournamentWebApp.Entities.Domain;

namespace TournamentWebApp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                          .SetBasePath(env.ContentRootPath)
                          .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
        }
        public IConfiguration Configuration { get; set; }
        public IServiceProvider ServiceProvider { get; set; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddScoped(_ => new TournamentDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<ITournamentRepository, TournamentRepository.Repository.TournamentRepository>();
            services.AddScoped<IEventRepository, TournamentRepository.Repository.EventRepository>();
            services.AddScoped<IEventDetailRepository, TournamentRepository.Repository.EventDetailsRepository>();
            services.AddScoped<IEventStatusRepository, TournamentRepository.Repository.EventDetailStatusRepository>();
            services.AddDbContext<ApplicationUserDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationUserDbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            
            app.UseFileServer();
            app.UseNodeModules(env.ContentRootPath);
            app.UseIdentity();
            app.UseMvc(ConfigureRoutes);
            Task.Run(()=> CreateRoles());
        }
        private async Task CreateRoles()
        {
            try
            {
                var roleManager = ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var userManager = ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roles = Configuration["ApplicationSettings:Roles"];
                var roleNames = roles.Split(',');
                IdentityResult roleResult;
                foreach (var roleName in roleNames)
                {
                    var isRoleExist = await roleManager.RoleExistsAsync(roleName);
                    if (!isRoleExist)
                    {
                        roleResult = await roleManager.CreateAsync(new IdentityRole(roleName));
                    }
                }

                var superUser = new ApplicationUser
                {
                    UserName = Configuration["ApplicationSettings:SuperUser:Username"],
                    Email = Configuration["ApplicationSettings:SuperUser:Username"]
                };
                var superUserPwd = Configuration["ApplicationSettings:SuperUser:Password"];
                var userExist = await userManager.FindByEmailAsync(Configuration["ApplicationSettings:SuperUser:Username"]);
                if (userExist == null)
                {
                    var createSuperUser = await userManager.CreateAsync(superUser, superUserPwd);
                    if (createSuperUser.Succeeded)
                    {
                        var adminRole = roleNames.Where(c => c == "Administrator").First();
                        await userManager.AddToRoleAsync(superUser, adminRole);
                    }
                }
            }
            catch (Exception)
            {

                throw new Exception("System encountered error. Could not create user roles");
            }


        }
        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Defaults", "{controller=Home}/{action=Index}/{id?}");
            routeBuilder.MapRoute("Tournament", "Tournament/{Controller=Tournament}/{action=Create}/{id?}");
        }
    }
}
