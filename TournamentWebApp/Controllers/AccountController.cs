﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TournamentWebApp.Entities.Domain;
using TournamentWebApp.Entities.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TournamentWebApp.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;

        // GET: /<controller>/
        public AccountController(UserManager<ApplicationUser> userManager,
                                  SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var loginResult = await _signInManager
                        .PasswordSignInAsync(model.Username, model.Password, model.rememberMe, false);
                    if (loginResult.Succeeded)
                    {
                        if (Url.IsLocalUrl(model.ReturnUrl))
                        {
                            return Redirect(model.ReturnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    ModelState.AddModelError("", "Could not login at this time, kindly try again later");
                }
                return View(model);
            }
            catch (Exception ex)
            {

                ViewData["Message"] = "Error occurred. Try again later";
                return View(model);
            }
        }
        public async Task<IActionResult> SignOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var newUser = new ApplicationUser
                    {
                        UserName = model.Email,
                        Email = model.Email
                    };
                    var userCreateResult = await _userManager.CreateAsync(newUser, model.Password);

                    if (userCreateResult.Succeeded)
                    {
                        await _userManager.AddToRoleAsync(newUser, "User");
                        return RedirectToAction("Registered", new { Email = model.Email });
                    }
                    else
                    {
                        foreach(var err in userCreateResult.Errors)
                        {
                            ModelState.AddModelError("", err.Description);
                        }
                    }

                }
                return View();
            }
            catch (Exception ex)
            {

                ViewData["Message"] = "Error occurred. Try again later";
                return View(model);
            }
        }
        public IActionResult Registered(RegisteredViewModel model)
        {
            return View(model);
        }
    }
}
