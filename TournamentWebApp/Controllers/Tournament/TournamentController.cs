﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TournamentRepository.IRepository;
using TournamentWebApp.Entities.ViewModels;
using TournamentModels.Domains;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TournamentWebApp.Controllers.Tournament
{
    [Authorize]
    public class TournamentController : Controller
    {
        private ITournamentRepository _tournamentRepository;

        // GET: /<controller>/
        public TournamentController(ITournamentRepository tournamentRepository)
        {
            _tournamentRepository = tournamentRepository;
        }
        [HttpGet]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Create()
        {
            try
            {
                //Get all tournaments available in the database
                var AllTournaments = await _tournamentRepository.GetAllTournaments();
                //pass this list to the client
                ViewData["AllTournaments"] = AllTournaments.ToList();
                return View();
            }
            catch (Exception ex)
            {
                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create(TournamentViewModel model)
        {
            try
            {
                // Get all tournaments from database
                var AllTournaments = new List<TournamentModels.Domains.Tournament>();
                //check if model state is valid and entries from the user is valid
                if (ModelState.IsValid)
                {
                    var newTournament = new TournamentModels.Domains.Tournament
                    {
                        TournamentName = model.TournamentName
                    };
                    await _tournamentRepository.InsertTournament(newTournament);
                    AllTournaments = (await _tournamentRepository.GetAllTournaments()).ToList();
                    ViewData["Message"] = "Tournament successfully created";
                    ViewData["AllTournaments"] = AllTournaments;
                    return View();
                    
                }
                AllTournaments = (await _tournamentRepository.GetAllTournaments()).ToList();
                ViewData["AllTournaments"] = AllTournaments;
                ModelState.AddModelError("", "Tournament was not created. Kindly check your entries");
                return View(model);
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View(model);
            }
        }
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                // Get tournament to update
                var tournament = await _tournamentRepository.GetTournamentById(id);
                if (tournament != null)
                {
                    //send the tournament to update to the client
                    var tournamentViewmodel = new TournamentViewModel
                    {
                        TournamentName = tournament.TournamentName
                    };
                    return View(tournamentViewmodel); 
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,TournamentViewModel model)
        {
            try
            {
                //Check if modelState is vails
                if (ModelState.IsValid)
                {
                    //Get tournament to update
                    var UpdateTournament = await _tournamentRepository.GetTournamentById(id);
                    if(UpdateTournament != null)
                    {
                        UpdateTournament.TournamentName = model.TournamentName;
                        await _tournamentRepository.UpdateTournament(UpdateTournament);
                        ViewData["Message"] = "Tournament successfully updated";
                        return View(model);
                    }
                    ViewData["Message"] = "Incorrect request";
                    return View();
                }
                ModelState.AddModelError("", "Tournament was not edited.Kindly check entries");
                return View(model);
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                // Get tournament to delete
                var tournament = await _tournamentRepository.GetTournamentById(id);
                if(tournament != null)
                {
                    //Delete tournament
                    await _tournamentRepository.DeleteTournament(tournament);
                    return RedirectToAction("Create");
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
    }
}
