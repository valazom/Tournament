﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TournamentModels.Domains;
using TournamentRepository.IRepository;
using TournamentWebApp.Entities.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TournamentWebApp.Controllers.Tournament
{
    [Authorize]
    public class EventDetailStatusController : Controller
    {
        private IEventStatusRepository _eventStatusRepository;
        private IEventDetailRepository _eventDetailRepository;

        // GET: /<controller>/
        public EventDetailStatusController(IEventStatusRepository eventStatusRepository,
            IEventDetailRepository eventDetailRepository)
        {
            _eventStatusRepository = eventStatusRepository;
            _eventDetailRepository = eventDetailRepository;
        }
        [HttpGet]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Create(int id)
        {
            try
            {
                // Get all event status
                var allEventStatus = await _eventStatusRepository.GetAllEventStatus();
                //Send to the client
                ViewData["AllEventStatus"] = allEventStatus.ToList();
                var eventDetail = await _eventDetailRepository.GetEventDetailById(id);
                ViewData["eventID"] = eventDetail.FK_EventID;
                ViewData["eventDetailID"] = id;
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create(int id,EventDetailStatusViewModel model)
        {
            try
            {
                var allEventStatus = new List<EventDetailStatus>();
                // Get the event Detail
                var eventDetail = await _eventDetailRepository.GetEventDetailById(id);
                ViewData["eventID"] = eventDetail.FK_EventID;
                ViewData["eventDetailID"] = id;
                if (ModelState.IsValid)
                {
                    //Create a new EventStatus
                    var eventStatus = new EventDetailStatus
                    {
                        EventDetailStatusName = model.EventDetailStatusName
                    };
                    //Insert the status to the DB
                    await _eventStatusRepository.InsertEventStatus(eventStatus);
                    allEventStatus = (await _eventStatusRepository.GetAllEventStatus()).ToList();
                    ViewData["AllEventStatus"] = allEventStatus;
                    
                    ViewData["Message"] = "Successfully created EventStatus";
                    return View();
                }
                allEventStatus = (await _eventStatusRepository.GetAllEventStatus()).ToList();
                ViewData["AllEventStatus"] = allEventStatus;
                ModelState.AddModelError("","Error in creating status.Kindly check your entries");
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Add(int eventDetailId, int eventStatusId)
        {
            try
            {
                // Get the event Detail
                var eventDetail = await _eventDetailRepository.GetEventDetailById(eventDetailId);
                if(eventDetail != null)
                {
                    //Update the eventDetail with Status ID
                    eventDetail.FK_EventDetailStatusID = (short)eventStatusId;
                    await _eventDetailRepository.UpdateEventDetail(eventDetail);
                    return RedirectToAction("Create", "EventDetails", new { id = eventDetail.FK_EventID });
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(int eventDetailId, int eventStatusId)
        {
            try
            {
                var eventStatus = await _eventStatusRepository.GetEventDetailStatusById((Int16)eventStatusId);
                if(eventStatus != null)
                {
                    await _eventStatusRepository.DeleteEventDetailStatus(eventStatus);
                    return RedirectToAction("Create", new { id = eventDetailId });
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
    }
}
