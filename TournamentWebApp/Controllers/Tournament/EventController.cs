﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TournamentModels.Domains;
using TournamentRepository.IRepository;
using TournamentWebApp.Entities.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TournamentWebApp.Controllers.Tournament
{
    [Authorize]
    public class EventController : Controller
    {
        private IEventRepository _eventRepository;
        private ITournamentRepository _tournamentRepository;

        // GET: /<controller>/
        public EventController(IEventRepository eventRepository,
            ITournamentRepository tournamentRepository)
        {
            _eventRepository = eventRepository;
            _tournamentRepository = tournamentRepository;
        }
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Create(int id)
        {
            try
            {
                //Get all Events
                var AllEvents = await _eventRepository.GetEventsByTournamentId(id);
                if(AllEvents != null)
                {
                    //Send All events to the client
                    ViewData["AllEvents"] = AllEvents.ToList();
                    return View();
                }
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Create(int id,EventViewModel model)
        {
            try
            {
                var AllEvents = new List<Event>();
                if (ModelState.IsValid)
                {
                    //Get the tournament for which you are creating the event for
                    var tournament = await _tournamentRepository.GetTournamentById(id);
                    if (tournament != null)
                    {
                        //Create event to insert
                        var newEvent = new TournamentModels.Domains.Event
                        {
                            FK_TournamentID = tournament.TournamentID,
                            EventName = model.EventName,
                            EventNumber = (short)model.EventNumber,
                            EventDateTime = model.EventDateTime,
                            EventEndDateTime = model.EventEndDateTime,
                            AutoClose = model.AutoClose
                        };
                        // insert event 
                        await _eventRepository.InsertEvent(newEvent);
                        AllEvents = (await _eventRepository.GetAllEvents()).ToList();
                        ViewData["AllEvents"] = AllEvents;
                        return View();

                    }
                    ViewData["Message"] = "Incorrect request";
                    return View();
                }
                AllEvents = (await _eventRepository.GetAllEvents()).ToList();
                ViewData["AllEvents"] = AllEvents;
                ModelState.AddModelError("", "Error in inserting Event.Kindly check entries");
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpGet]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                //Get event to update
                var updateEvent = await _eventRepository.GetEventById(id);
                if(updateEvent != null)
                {
                    //populate the view model to send to the client
                    var model = new EventViewModel
                    {
                        EventName = updateEvent.EventName,
                        EventNumber = updateEvent.EventNumber,
                        EventDateTime = updateEvent.EventDateTime,
                        EventEndDateTime = (DateTime)updateEvent.EventEndDateTime,
                        AutoClose = updateEvent.AutoClose
                    };
                    ViewData["ID"] = updateEvent.FK_TournamentID;
                    return View(model);
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Edit(int id,EventViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //get event to update
                    var updateEvent = await _eventRepository.GetEventById(id);
                    if(updateEvent != null)
                    {
                        updateEvent.EventName = model.EventName;
                        updateEvent.EventNumber = (short)model.EventNumber;
                        updateEvent.EventDateTime = model.EventDateTime;
                        updateEvent.EventEndDateTime = model.EventEndDateTime;
                        updateEvent.AutoClose = model.AutoClose;

                        await _eventRepository.UpdateEvent(updateEvent);
                        ViewData["Message"] = "Successfully updated Event";
                        ViewData["ID"] = updateEvent.FK_TournamentID;
                        return View(model);
                    }
                    ViewData["Message"] = "Incorrect request";
                    return View();
                }
                ModelState.AddModelError("", "Error in updating Event.Kindly check entries");
                return View(model);
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var dEvent = await _eventRepository.GetEventById(id);
                if(dEvent != null)
                {
                    await _eventRepository.DeleteEvent(dEvent);
                    return RedirectToAction("Create",new {id = dEvent.FK_TournamentID});
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
    }
}
