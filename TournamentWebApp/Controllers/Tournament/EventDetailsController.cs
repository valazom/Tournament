﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TournamentModels.Domains;
using TournamentRepository.IRepository;
using TournamentWebApp.Entities.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TournamentWebApp.Controllers.Tournament
{
    [Authorize]
    public class EventDetailsController : Controller
    {
        private IEventDetailRepository _eventDetailRepository;
        private IEventRepository _eventRepository;

        // GET: /<controller>/

        public EventDetailsController(IEventDetailRepository eventDetailRepository,
            IEventRepository eventRepository)
        {
            _eventDetailRepository = eventDetailRepository;
            _eventRepository = eventRepository;
        }
        [HttpGet]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Create(int id)
        {
            try
            {
                //Get Event Details to change its status
                var AllEventDetails = await _eventDetailRepository.GetEventDetailByEventId(id);
                //get Event for the specified eventdetail
                var events = await _eventRepository.GetEventById(id);
                //Send this ID to the client
                ViewData["ID"] = events.FK_TournamentID;

                if(AllEventDetails != null)
                {
                    ViewData["AllEventDetails"] = AllEventDetails.ToList();
                    
                    return View();
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Create(int id, EventDetailsViewModel model)
        {
            try
            {

                var AllEventails = new List<EventDetail>();
                //Get Event for the EventDetail
                var newEvent = await _eventRepository.GetEventById(id);
                ViewData["ID"] = newEvent.FK_TournamentID;
                if (ModelState.IsValid)
                {
                    
                    if (newEvent != null)
                    {
                        //Create a new event detail
                        var eventDetail = new EventDetail
                        {
                            FK_EventID = newEvent.EventID,
                            EventDetailName = model.EventDetailName,
                            EventDetailNumber = (short)model.EventDetailNumber,
                            EventDetailOdd = model.EventDetailOdd,
                            FinishingPosition = model.FinishingPosition,
                            FirstTimer = model.FirstTimer
                        };
                        //insert the event detail to the DB
                        await _eventDetailRepository.InsertEventDetail(eventDetail);
                        AllEventails = (await _eventDetailRepository.GetAllEventDetails()).ToList();
                        //send this list to the client
                        ViewData["AllEventDetails"] = AllEventails;
                        ViewData["Message"] = "Successfully created the eventDetails";
                        return View();
                    }
                    ViewData["Message"] = "Incorrect request";
                    return View();
                }
                // Even though the insert was not successful, still download 
                // AllEventdetails
                AllEventails = (await _eventDetailRepository.GetAllEventDetails()).ToList();
                ViewData["AllEventDetails"] = AllEventails;
                ModelState.AddModelError("", "Error in creating event details check your entries");
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpGet]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var eventDetail = await _eventDetailRepository.GetEventDetailById(id);
                if(eventDetail != null)
                {
                    var model = new EventDetailsViewModel
                    {
                        EventDetailName = eventDetail.EventDetailName,
                        EventDetailNumber = eventDetail.EventDetailNumber,
                        EventDetailOdd = eventDetail.EventDetailOdd,
                        FinishingPosition = eventDetail.FinishingPosition,
                        FirstTimer = eventDetail.FirstTimer
                    };
                    ViewData["ID"] = eventDetail.FK_EventID;
                    return View(model);
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,User")]
        public async Task<IActionResult> Edit(int id, EventDetailsViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var eventDetail = await _eventDetailRepository.GetEventDetailById(id);
                    if(eventDetail != null)
                    {
                        eventDetail.EventDetailName = model.EventDetailName;
                        eventDetail.EventDetailNumber = (short)model.EventDetailNumber;
                        eventDetail.EventDetailOdd = model.EventDetailOdd;
                        eventDetail.FinishingPosition = model.FinishingPosition;
                        eventDetail.FirstTimer = model.FirstTimer;

                        await _eventDetailRepository.UpdateEventDetail(eventDetail);
                        ViewData["Message"] = "Successfully updated the Event Detail";
                        ViewData["ID"] = eventDetail.FK_EventID;
                        return View(model);
                    }
                    ViewData["Message"] = "Incorrect request";
                    return View();
                }
                ModelState.AddModelError("", "Error in Updating Event details");
                return View(model);
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var eventDetail = await _eventDetailRepository.GetEventDetailById(id);
                if(eventDetail != null)
                {
                    await _eventDetailRepository.DeleteEventDetail(eventDetail);
                    return RedirectToAction("Create", new { id = eventDetail.FK_EventID });
                }
                ViewData["Message"] = "Incorrect request";
                return View();
            }
            catch (Exception)
            {

                ViewData["Message"] = "An Error occurred. try again";
                return View();
            }
        }
    }
}
