﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentModels.Domains
{
    public class EventDetailStatus
    {
        [Key]
        [Column(TypeName = "smallint")]
        public Int16 EventDetailStatusID { get; set; }
        [Required]
        [Column(TypeName = "varchar")]
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string EventDetailStatusName { get; set; }
        

    }
}
