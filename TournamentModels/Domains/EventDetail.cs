﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentModels.Domains
{
    public class EventDetail
    {
        [Key]
        [Column(TypeName = "bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventDetailID { get; set; }
        [Required]
        [Column(TypeName = "bigint")]
        public int FK_EventID { get; set; }
        [Required]
        [Column(TypeName = "smallint")]
        public Int16 FK_EventDetailStatusID { get; set; }
        [Required]
        [Column(TypeName = "varchar")]
        [StringLength(50)]
        public string EventDetailName { get; set; }
        [Required]
        [Column(TypeName = "smallint")]
        public Int16 EventDetailNumber { get; set; }
        [Required]
        public decimal EventDetailOdd { get; set; }
        [Column(TypeName = "smallint")]
        public Int16 FinishingPosition { get; set; }
        [Required]
        public bool FirstTimer { get; set; }
        


    }
}
