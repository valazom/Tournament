﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentModels.Domains
{
    public class Tournament
    {
        [Key]
        [Column(TypeName ="bigint")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TournamentID { get; set; }
        [Required]
        [Column(TypeName = "varchar")]
        [StringLength(200)]
        public string TournamentName { get; set; }
        

    }
}
