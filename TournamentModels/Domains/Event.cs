﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentModels.Domains
{
    public class Event
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int EventID { get; set; }
        [Column(TypeName = "bigint")]
        [Required]
        public int FK_TournamentID { get; set; }
        [Column(TypeName = "varchar")]
        [StringLength(100)]
        [Required]
        public string EventName { get; set; }
        [Column(TypeName = "smallint")]
        [Required]
        public Int16 EventNumber { get; set; }
        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime EventDateTime { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? EventEndDateTime { get; set; }
        [Required]
        public bool AutoClose { get; set; }
        
        

    }
}
